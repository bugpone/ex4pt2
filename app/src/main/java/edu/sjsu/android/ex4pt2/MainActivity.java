package edu.sjsu.android.ex4pt2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import edu.sjsu.android.ex4pt2.R;

public class MainActivity extends AppCompatActivity {

    private Button webButton;
    private Button callButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        webButton = (Button) findViewById(R.id.web);
        callButton = (Button) findViewById(R.id.call);

        webButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "http://www.example.com";
                Intent webIntent = new Intent(Intent.ACTION_VIEW,Uri.parse(url));
                Intent chooser = Intent.createChooser(webIntent, "Pick browser");
                if (webIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(chooser);
                }
            }
        });

        callButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("MDEBUG","BUTTON CLICK");
                String myPhoneNumberUri ="tel:+194912344444";
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(myPhoneNumberUri));
                startActivity(intent);
            }
        });
    }
}
